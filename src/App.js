import gUserInfo from "./info";
function App() {

  
  return (
    <div className="user-infor">
      <h5>Họ và Tên: {gUserInfo.firstname} {gUserInfo.lastname}</h5>
      <img src={gUserInfo.avatar} width={300} alt="logo"/>
      <p>Tuổi của user là {gUserInfo.age}</p>
      <p> {gUserInfo.age< 35?"Anh ấy còn trẻ":"Anh ấy đã già"}</p>
      <ul>Ngoại ngữ
       {gUserInfo.language.map(function(element, index){
        return <li key={index}>{element}</li>
       })}
      </ul>
    </div>
  );
}

export default App;
