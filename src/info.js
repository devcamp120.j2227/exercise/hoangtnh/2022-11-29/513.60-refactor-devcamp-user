import avartar from "./assets/images/avatardefault_92824.webp"
const gUserInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: avartar ,
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
  }
  export default gUserInfo